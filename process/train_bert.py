import sys
import os
project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
import torch.optim as optim
from tqdm import tqdm
import pandas as pd
import torch
from torch.utils.data import DataLoader
import torch.nn as nn
from sklearn.model_selection import train_test_split
import numpy as np
from transformers import BertTokenizer, BertModel
from util.model_manager import DNASequenceClassifier
from util.data_manager import DNADataset

kmer = 6
window_size = 200
motif_size = 12


num_classes = 2  # Binary classification
model_name = "zhihan1996/DNA_bert_"+str(kmer)
model = BertModel.from_pretrained(model_name, num_labels=2, finetuning_task="dnaprom", cache_dir=None)
tokenizer = BertTokenizer.from_pretrained(model_name, trust_remote_code=True)
clf_model = DNASequenceClassifier(model, None, num_classes)

try:
    dataset = pd.read_csv('sim_data_ws{}_motif{}.csv'.format(window_size, motif_size))
except FileNotFoundError:
    print('run simulated_data_generator.py with the selected window_size and motif_size')

X = dataset['seq']
Y = np.eye(2)[pd.cut(dataset['label'], bins=2, labels=[0, 1], right=False)]

x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.1, random_state=None)

# Set batch size and max sequence length
batch_size = 32
labels = torch.tensor(y_train, dtype=torch.float32)
dataset = DNADataset(x_train.tolist(), labels, tokenizer, window_size, kmer)
data_loader_train = DataLoader(dataset, batch_size=batch_size, shuffle=True)

labels = torch.tensor(y_test, dtype=torch.float32)
dataset = DNADataset(x_test.tolist(), labels, tokenizer, window_size, kmer)
data_loader_test = DataLoader(dataset, batch_size=batch_size, shuffle=True)

optimizer = optim.Adam(clf_model.parameters(), lr=1e-5)
criterion = nn.BCELoss()
clf_model.to('cuda')
# Set the number of epochs
num_epochs = 5
total_samples = len(data_loader_train.dataset)  # Total number of samples
batch_size = data_loader_train.batch_size  # Batch size
batch_size_test = data_loader_test.batch_size
total_samples_test = len(data_loader_test.dataset)
# Training loop
for epoch in range(num_epochs):
    clf_model.train()
    total_loss = 0
    total_correct = 0
    total_processed = 0  # To keep track of total processed samples
    progress_bar = tqdm(data_loader_train, desc=f"Epoch {epoch+1}/{num_epochs}", leave=False)
    for batch in progress_bar:
        input_ids = batch["input_ids"].to('cuda')
        attention_mask = batch["attention_mask"].to('cuda')
        target_labels = batch["label"].to('cuda')
        optimizer.zero_grad()
        logits, _ = clf_model(input_ids, attention_mask)
        loss = criterion(logits, target_labels)
        loss.backward()
        optimizer.step()
        predictions = logits.argmax(dim=1)
        total_correct += (predictions == target_labels.argmax(dim=1)).sum().item()
        total_processed += batch_size
        if total_processed%(10*batch_size) == 0:
            b_acc = total_correct / total_processed
            progress_percentage = (total_processed / total_samples) * 100
            progress_bar.set_postfix(loss=loss.item(), batch_acc=b_acc, progress=f"{progress_percentage:.2f}%")
        total_loss += loss.item()
    epoch_accuracy = total_correct / total_samples
    average_loss = total_loss / len(data_loader_train)
    print(f"Epoch [{epoch+1}/{num_epochs}] - Average Loss: {average_loss:.4f}, Epoch Accuracy: {epoch_accuracy:.4f}")
    # Evaluate the model on test data
clf_model.eval()
test_correct = 0
with torch.no_grad():
    for batch in data_loader_test:
        input_ids = batch["input_ids"].to('cuda')
        attention_mask = batch["attention_mask"].to('cuda')
        target_labels = batch["label"].to('cuda')
        logits, _ = clf_model(input_ids, attention_mask)
        predicted_labels = logits.argmax(dim=1)
        test_correct += (predicted_labels == target_labels.argmax(dim=1)).sum().item()
test_accuracy = test_correct / total_samples_test
with open("bert_classification_result.txt", "a") as file_object:
    file_object.write('%s\t%s\t%s\t%s' %(str(kmer), str(window_size), str(motif_size), str(test_accuracy)))
    file_object.write("\n")

clf_model.save("./dump_files/pretrained_models/" + "kmer_"+str(kmer) + "_ws_" + str(window_size) + "_KO_" + str(motif_size))
