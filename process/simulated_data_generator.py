import pandas as pd
import numpy as np

np.random.seed(42)

def generate_sequence(label, seq_size, motif_size):
    seq = ''.join(np.random.choice(list('ACGT'), seq_size))
    if label == 1:
        idx = np.random.randint(0, seq_size-motif_size+1)
        seq = seq[:idx] + 'AACCTTGGTTAA' + seq[idx+motif_size:]
    return seq

n = 50000
window_size = 200
motif_size = 12

labels = np.array([0] * (n // 2) + [1] * (n // 2))

df = pd.DataFrame({'label': labels})
df['seq'] = df['label'].apply(lambda x: generate_sequence(x, window_size, motif_size))

df.sample(frac=1).to_csv('sim_data_ws{}_motif{}.csv'.format(window_size, motif_size), index=False)
