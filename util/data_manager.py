import sys
import os
project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
from torch.utils.data import Dataset
from preprocess import preprocess

class DNADataset(Dataset):
    def __init__(self, sequences, labels, tokenizer, max_length, kmer):
        self.sequences = sequences
        self.labels = labels
        self.tokenizer = tokenizer
        self.max_length = max_length
        self.kmer = kmer
    def __len__(self):
        return len(self.sequences)
    def __getitem__(self, idx):
        sequence = self.sequences[idx]
        label = self.labels[idx]
        # Tokenize sequence
        encoded_sequence = self.tokenizer(
            preprocess.seq2kmer(sequence, int(self.kmer)),
            max_length=self.max_length,
            padding='max_length',
            truncation=True,
            return_tensors='pt'
        )
        input_ids = encoded_sequence["input_ids"].squeeze()
        attention_mask = encoded_sequence["attention_mask"].squeeze()
        return {
            "input_ids": input_ids,
            "attention_mask": attention_mask,
            "label": label,
            "sequence": sequence
        }
