import sys
import os
project_root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(project_root)
from torch.utils.data import DataLoader
import numpy as np
import torch
import pandas as pd
import model_manager as model_manager
from data_manager import DNADataset
from transformers import BertTokenizer

def model_eval(X, Y, model, tokenizer, window_size, kmer):
    batch_size = 32
    labels = torch.tensor(Y, dtype=torch.float32)
    dataset = DNADataset(X.tolist(), labels, tokenizer, window_size, kmer)
    data_loader = DataLoader(dataset, batch_size=batch_size, shuffle=True)
    model.to('cuda')
    model.eval()
    sequences = np.empty(len(dataset), dtype=object)
    labels = np.zeros(len(dataset))
    attention_scores = np.zeros([len(dataset), 12, 12, window_size, window_size])
    idx = 0
    with torch.no_grad():
        for batch in data_loader:
            input_ids = batch["input_ids"].to('cuda')
            attention_mask = batch["attention_mask"].to('cuda')
            o = model(input_ids=input_ids, attention_mask=attention_mask, output_attentions=True)
            sequences[idx:idx+len(input_ids)] = batch["sequence"]
            labels[idx:idx+len(input_ids)] = batch["label"].argmax(dim=1).numpy()
            attention_scores[idx:idx+len(input_ids)] = np.transpose(torch.stack(o[-1], dim=0).cpu().numpy(), (1, 0, 2, 3, 4))
            idx += len(input_ids)
    return sequences, labels.astype(int), attention_scores

kmer = 6
window_size = 200
motif_size = 12


model_name = "zhihan1996/DNA_bert_"+str(kmer)
tokenizer = BertTokenizer.from_pretrained(model_name, trust_remote_code=True)
fn = "/home/ssere004/projects/bert_attention/dump_files/pretrained_models/kmer_{}_ws_{}_KO_{}".format(kmer, window_size, motif_size)
clf_model = model_manager.load_clf_model(fn)


try:
    dataset = pd.read_csv('sim_data_ws{}_motif{}.csv'.format(window_size, motif_size))
except FileNotFoundError:
    print('run simulated_data_generator.py with the selected window_size and motif_size')

X = dataset['seq']
Y = np.eye(2)[pd.cut(dataset['label'], bins=2, labels=[0, 1], right=False)]

x_pos = X[np.where(Y[:, 1] == 1)[0]]
y_pos = Y[np.where(Y[:, 1] == 1)[0]]

sequences, labels, scores = model_eval(x_pos[0:1000], y_pos[0:1000], clf_model.bert, tokenizer, window_size, kmer)
np.save("/data/bert_attention/att_mtrixs/att_scores_n_100_ws_200_ms_12_kmer_6.npy", scores)
